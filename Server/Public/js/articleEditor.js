function getNewArticleKey(){
    return firebase.database().ref().child('articles').push().key;
}

function genNewImageKey(articleKey){
    var imageKey = firebase.database().ref().child('/image-keys/').push().key
    var updates = {};
    if(articleKey) {
        updates['/image-keys/' + articleKey] = imageKey;
    } else {
        updates['/image-keys/unasigned/'] = imageKey;
    }
    return imageKey;
}


/** Function that count occurrences of a substring in a string;
 * @param {String} string               The string
 * @param {String} subString            The sub string to search for
 * @param {Boolean} [allowOverlapping]  Optional. (Default:false)
 *
 * @author Vitim.us https://gist.github.com/victornpb/7736865
 * @see Unit Test https://jsfiddle.net/Victornpb/5axuh96u/
 * @see http://stackoverflow.com/questions/4009756/how-to-count-string-occurrence-in-string/7924240#7924240
 */
function occurrences(string, subString, allowOverlapping) {

    string += "";
    subString += "";
    if (subString.length <= 0) return (string.length + 1);

    var n = 0,
        pos = 0,
        step = allowOverlapping ? 1 : subString.length;

    while (true) {
        pos = string.indexOf(subString, pos);
        if (pos >= 0) {
            ++n;
            pos += step;
        } else break;
    }
    return n;
}

function longestCommonSubstring(a,b) {
    var longest = "";
    // loop through the first string
    for (var i = 0; i < a.length; ++i) {
        // loop through the second string
        for (var j = 0; j < b.length; ++j) {
            // if it's the same letter
            if (a[i] === b[j]) {
                var str = a[i];
                var k = 1;
                // keep going until the letters no longer match, or we reach end
                while (i+k < a.length && j+k < b.length // haven't reached end
                && a[i+k] === b[j+k]) { // same letter
                    str += a[i+k];
                    ++k;
                }
                // if this substring is longer than the longest, save it as the longest
                if (str.length > longest.length) { longest = str }
            }
        }
    }
    return longest;
}

function generateSearchIndexes(searchString){
    var searchTerms = searchString.toLowerCase().split(" ");
    $("#editor_article_list").find('a').each(function(){
        var score;
        var elem = $(this);
        // console.log(elem);


        var titleScore = 0;
        var locationScore = 0;
        searchTerms.forEach(function(searchTerm){
            locationScore = 0;
            var locations = elem.data('locationnames');
            locations.forEach(function(location){
                var match = longestCommonSubstring(searchTerm, location.toLowerCase());
                locationScore = Math.max(match.length, locationScore);
                // console.log(match);
            });
            // console.log(searchTerm + " l: " + locationScore);

            titleScore=0;
            var title = elem.html();
            titleScore = longestCommonSubstring(searchTerm, title.toLowerCase()).length;
            // console.log(searchTerm + " t: " + titleScore);
        });
        if(locationScore >= 4){
            $(this).addClass("location-match");
        } else {
            $(this).removeClass("location-match");
        }
        score = locationScore + titleScore;
        // console.log(score);
        $(this).data('searchIndex', score);
        console.log(score/searchString.length);
    });

}

function clearSearchHighlights(){
    $("#editor_article_list").find('a').each(function(){
        $(this).removeClass("location-match search-match-high search-match-medium search-match-low");
    });
}

function searchArticleList(searchString){
    if("" == searchString){
        clearSearchHighlights();
        sortChildren("#editor_article_list", "a", function(a,b){
            return a.innerHTML.localeCompare(b.innerHTML);
        });
        return;
    }
    console.log("searching for: " + searchString);
    generateSearchIndexes(searchString);

    sortChildren("#editor_article_list", "a", function(a,b){
        return $(a).data('searchIndex') < ($(b).data('searchIndex'));
    });
}

function searchArticleListOnInput(){
    searchArticleList($("#editor_article_search").val());
}

//TODO optional Add icon panorama/fisheye as indicator for saved/unsaved
function isArticleChanged(){
    return !($("#save-article-button").hasClass("disabled"));
}

function setArticleChanged(val){
    if(!val){
        $("#save-article-button").addClass("disabled");
    } else {
        $("#save-article-button").removeClass("disabled");
    }
}


function newArticle(){
    setArticleEditorContents({title:"", body:"<p><br></p>", locations:[], key:getNewArticleKey()});
    $("#article_title").focus();
    status("succes", "New article");
}

function articleListView(articleHeader){
    var locationsString = JSON.stringify(articleHeader.locationnames);
    // console.log(locationsString);
    return "<a id='" + articleHeader.key + "' href='#!' class='collection-item' data-locationnames='" + locationsString + "'>"+ articleHeader.title +"</a>";
    // return "<a id='" + articleHeader.key + "' href='#!' class='collection-item'>"+ articleHeader.title +"</a>";
}

function onNewDatabaseArticle(articleHeader){
    $("#editor_article_list").append(articleListView(articleHeader));
    // $("#"+articleHeader.key).click(articleHeader, loadArticleByPackedKey);
    $("#"+articleHeader.key).click(articleHeader, startLoadNewArticle);
    sortChildren("#editor_article_list", "a", function(a,b){
        return a.innerHTML.localeCompare(b.innerHTML);
    });
}

function onUpdateDatabaseArticle(articleHeader){
    // $("#editor_article_list").find($("#"+articleHeader.key)).html(articleHeader.title);
    $("#editor_article_list").find($("#"+articleHeader.key)).replaceWith(articleListView(articleHeader));
    // $("#editor_expedition_list").find($("#"+expeditionHeader.key)).replaceWith(expeditionListView(expeditionHeader));
    $("#"+articleHeader.key).click(articleHeader, startLoadNewArticle);

    if(articleHeader.key == getArticleEditorContents().key){
        //TODO Ask whether to load in updated, keep editing current or save current as new article
        // loadArticleByKey(articleHeader.key);
    }
    sortChildren("#editor_article_list", "a", function(a,b){
        return a.innerHTML.localeCompare(b.innerHTML);
    });
}

function onRemoveDatabaseArticle(articleHeader){
    $("#editor_article_list").find($("#"+articleHeader.key)).remove();
}

function loadArticleByPackedKey(packedKey){
    loadArticleByKey(packedKey.data.key);
}

function articleLocationListView(locationHeader){
    // return "<p><input type='checkbox' class='filled-in' id='" + locationHeader.key + " data-locationnames=" + locationHeader.title + "'/><label for='" + locationHeader.key + "'>" + locationHeader.title + "</label></p>";
    return "<p><input type='checkbox' class='filled-in' id='" + locationHeader.key + "'/><label for='" + locationHeader.key + "'>" + locationHeader.title + "</label></p>";
}

function onNewDatabaseArticleLocation(locationHeader){
    $("#article_locations").append(articleLocationListView(locationHeader));
    $("#"+locationHeader.key).change(function (){
        setArticleChanged(true);
    });

    sortChildren("#article_locations", "p", function(a,b){
        return $(a).find("label").html().localeCompare($(b).find("label").html());
    });

    // $("#article_locations p input").change(function(){
    //     setArticleChanged(true);
    // });
    // $("#"+locationHeader.key).click(locationHeader, toggleLocationForActiveArticle);
}

function onUpdateDatabaseArticleLocation(locationHeader){
    $("#article_locations").find($("p #"+locationHeader.key).siblings("label")).html(locationHeader.title);
    sortChildren("#article_locations", "p", function(a,b){
        return $(a).find("label").html().localeCompare($(b).find("label").html());
    });
}

function onRemoveDatabaseArticleLocation(locationHeader){
    $("#article_locations").find($("#"+locationHeader.key)).remove();
}

function startLoadNewArticle(packedKey){
    var key = null;
    if(packedKey){
        key = packedKey.data.key;
    }
    if(isArticleChanged()){
        var onModalComplete = function(){
            loadArticleByKey(key);
        };

        $('#modalAskSave').modal("open", {
            dismissible: false,
            complete: onModalComplete
        }).modal("open");
    }
    loadArticleByKey(key);
}

//Binds to /articles/
function bindArticleListViewPreload(){
    var articlesRef = firebase.database().ref('/articles/');
    articlesRef.off();
    articlesRef.on('child_added', function(data) {
        var header = {title:data.val().title, locationnames:data.val().locationnames, key:data.key};
        onNewDatabaseArticle(header);
        updateActiveArticleView();
        setTimeout(function(){
            subtleFlashElement(data.key, "addedColor", 800, null);
        }, 400);
    });

    articlesRef.on('child_changed', function(data) {
        var header = {title:data.val().title, locationnames:data.val().locationnames, key:data.key};
        onUpdateDatabaseArticle(header);
        Materialize.toast("Article&nbsp;<em>" + data.val().title + "</em>&nbsp;has been updated", 4000);
        subtleFlashElement(data.key, "changedColor", 1200, null);
    });

    articlesRef.on('child_removed', function(data) {
        var header = {title:data.val().title, locationnames:data.val().locationnames, key:data.key};
        Materialize.toast("Article&nbsp;<em>" + data.val().title + "</em>&nbsp;has been removed", 4000);
        subtleFlashElement(data.key, "deletedColor", 1200, function(){
            onRemoveDatabaseArticle(header);
            updateActiveArticleView();
        })
    });
}

function bindArticleLocations(){
    var locationsRef = firebase.database().ref('/location-titles/');
    locationsRef.off();
    locationsRef.on('child_added', function(data) {
        onNewDatabaseArticleLocation({title:data.val(), key:data.key});
        // updateActiveArticleView();
        setTimeout(function(){
            subtleFlashElement(data.key, "addedColor", 800, null);
        }, 400);
    });

    locationsRef.on('child_changed', function(data) {
        onUpdateDatabaseArticleLocation({title:data.val(), key:data.key});
        Materialize.toast("Location&nbsp;<em>" + data.val() + "</em>&nbsp;has been updated", 4000);
        subtleFlashElement(data.key, "changedColor", 1200, null);
    });

    locationsRef.on('child_removed', function(data) {
        Materialize.toast("Location&nbsp;<em>" + data.val() + "</em>&nbsp;has been removed", 4000);
        subtleFlashElement(data.key, "deletedColor", 1200, function(){
            onRemoveDatabaseArticleLocation({title:data.val(), key:data.key});
            // updateActiveArticleView();
        })
    });
}

function bindArticles(){
    if(config.articleLoadMode == "preload"){
        bindArticleListViewPreload();
    } else {
        bindArticleListView();
    }
}

function loadArticleByKey(key){
    if(null == key){
        newArticle();
        return;
    }
    setLoading(true);
    firebase.database().ref('/articles/' + key).once('value').then(function(snapshot) {
        if(null == snapshot.val()){
            status("error", "failed to retrieve article based on key: " +key);
            setLoading(false);
            return;
        }
        var data = {
            title: snapshot.val().title,
            body: snapshot.val().body,
            locations: snapshot.val().locations,
            key: key
        }
        setArticleEditorContents(data);
        setLoading(false);
        status("succes", "loaded article: " +snapshot.val().title);
    });
}
function deleteDatabaseArticleByKey(key){
    backUp('/articles/' + key);
    backUp('/article-titles/' + key);
    setTimeout(function(){
        firebase.database().ref('/articles/' + key).remove();
        firebase.database().ref('/article-titles/' + key).remove();
    }, 300);
}

function updateActiveArticleView(){
    var key = $("#article_key").html();
    $("#editor_article_list").find(".active").removeClass("active");
    $("#"+key).addClass("active");
}

function getArticleLocations(){
    var locations = [];
    $("#article_locations").find("*:checked").each(function(){
        locations.push(this.id);
    });
    console.log("Selected location keys: " + locations);
    return locations;
}

function getArticleLocationNames(){
    var locations = [];
    $("#article_locations").find("*:checked").each(function(){
        locations.push($(this).siblings('label').html());
    });
    console.log("Selected location names: " + locations);
    return locations;
}


function setArticleLocations(locations){
    $("#article_locations").children().children().prop("checked", false);
    if(locations){
        for(var i=0; i<locations.length; i++){
            $("#"+locations[i]).prop('checked', true);
        }
    }
}

function storeImage(articleKey, imageKey, dataUri) {
    firebase.storage().ref('/article-images/' + articleKey + "/" + imageKey).putString(dataUri, 'data_url').then(function (snapshot) {
        console.log("Stored image imageKey " + imageKey);
    });
}

function storeArticleEditorsImages(){
    var innerHtml = getArticleBody();
    var key = getArticleKey();

    var storageRef = firebase.storage().ref('/article-images/' + key);

    $('img', $(innerHtml).context).each(function(index){
        console.log("Uploading image " + index);
        var base64Image = $(this).attr('src');
        // var abc = new Image();
        // abc.src=base64Image;
        // document.body.appendChild(abc);
        firebase.storage().ref('/article-images/' + key + "/" + index).putString(base64Image, 'data_url').then(function(snapshot) {
            console.log('Uploaded a data_url string!');
        });
    });
}

function getArticleTextBody(){
    var articleKey = getArticleKey();

    $('.ql-editor').find('img').each(function(){
        var isSaved = $(this).data('stored');
        if(!isSaved){
            var data = $(this).attr('src');
            var imageKey = genNewImageKey(articleKey);
            var dimen = getBase64ImageDimensions(data);

            var height = $(this).height();
            var width = $(this).width();
            console.log(height + ";" + width);

            storeImage(articleKey, imageKey, data);

            $(this).height(height);
            $(this).width(width);
            $(this).attr('id', imageKey);
            $(this).attr("data-stored","true");
            $(this).addClass("stored");
        }

    });

    var $html = $('<div />',{html:getArticleBody()});
    $html.find('img').each(function (){
        this.src =  "";
    });
    return $html.html();
}

function loadArticleImages(articleKey){
    // var articleKey = getArticleKey();
    $('.ql-editor').find('img').each(function(){
        var isSaved = $(this).data('stored');
        if(isSaved){
            var ref = this;
            var imageKey = this.id;
            console.log("/article-images/" + articleKey + "/" + imageKey);
            var imageRef = firebase.storage().ref('/article-images/' + articleKey + "/" + imageKey);
            imageRef.getDownloadURL().then(function(url) {
                $(ref).attr("src", url);
                //Without setTimeout (delay = 0ms) it is too quick and the contents are loaded after setting it to false, which resets it to true
                setTimeout(function(){
                    // console.log("timeoutboy");
                    setArticleChanged(false);
                }, 0);
            }).catch(function(error) {
            });
        }
    });
}

function getArticleTitle(){
    return $("#article_title").val();
}

function getArticleBody(){
    return quill.root.innerHTML;
}

function getArticleKey(){
    return $("#article_key").html();
}

function getArticleEditorContents(){
    return {
        title: getArticleTitle(),
        body: getArticleTextBody(),
        locations : getArticleLocations(),
        locationnames: getArticleLocationNames(),
        key: getArticleKey()
    };
}

function setArticleEditorContents(articleData){
    $("#article_title").val(articleData.title);
    quill.root.innerHTML = articleData.body;
    $("#article_key").html(articleData.key);
    setArticleLocations(articleData.locations);
    updateActiveArticleView();
    loadArticleImages(articleData.key);

    //Without setTimeout (delay = 0ms) it is too quick and the contents are loaded after setting it to false, which resets it to true
    setTimeout(function(){
        // console.log("timeoutboy");
        setArticleChanged(false);
    }, 0);
}

function saveArticle(){
    setLoading(true);
    var editorContents = getArticleEditorContents();

    var updates = {};
    updates['/articles/' + editorContents.key] = editorContents;
    updates['/article-titles/' + editorContents.key] = editorContents.title;

    status("succes", "Saved article");
    setLoading(false);
    setArticleChanged(false);
    return firebase.database().ref().update(updates);
}

function deleteActiveArticle(){
    deleteDatabaseArticleByKey(getArticleEditorContents().key);
    newArticle();
}

function initArticleEditor(){

    quill = new Quill('#editor', {
        modules: {
            toolbar: [
                [{ header: [1, 2, false] }],
                ['bold', 'italic', 'underline'],
                ['image']
            ]
        },
        placeholder: 'Write an article',
        theme: 'snow'
    });

    // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
    bindArticles();
    bindArticleLocations();
    newArticle();
    quill.on('text-change', function(){
        if(quill.root.innerHTML != "<p><br></p>") {
            setArticleChanged(true);
        }
    });

    $("#article_title").on("input", function(){
        setArticleChanged(true);
    });

    $("#article_locations p input").change(function(){
        setArticleChanged(true);
    });

    $(window).bind('beforeunload', function (e) {
        if(isArticleChanged()){
            //This results in a box asking to stay/leave with the text you have unsaved changes
            return "aaa";
        }
        return;
    });

    $("#article_title").characterCounter();

}