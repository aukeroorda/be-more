

function logTableContents(){
    console.log(JSON.stringify({data: expeditionInfoTable.getData()}));
}
function getNewExpeditionKey(){
    return firebase.database().ref().child('expeditions').push().key;
}

function newExpedition(){
    setExpeditionEditorContents({title:"", group:"", website:"", location:"", infotable:JSON.stringify({d:emptyTableData()}), key:getNewExpeditionKey()});
    $("#expedition_title").focus();
    status("succes", "New expedition");
}

function expeditionListView(expeditionHeader){
    return "<li class='collection-item avatar' id='" + expeditionHeader.key + "'>"+
        "<img src='images/ic_account_circle_black_48px.svg' alt='' class='circle'>"+
            "<span class='title'> " + expeditionHeader.title + "</span>"+
            "<p>" + expeditionHeader.group + "<br>"+
                "<span class='date'><i class='material-icons small-icon-prefix'>flight_takeoff</i>"+expeditionHeader.departureDate + "</span>" +
            "</p>"+
            "<a href='mailto:"+ expeditionHeader.email +"?Subject="+ config.defaultMailSubject +"'><i class='material-icons amber-text'>mail</i></a>" +
            "<a href='//"+ expeditionHeader.website+"' target='_blank'><i class='material-icons amber-text'>language</i></a>" +
    "</li>";
}

function onNewDatabaseExpedition(expeditionHeader){
    $("#editor_expedition_list").append(expeditionListView(expeditionHeader));
    $("#"+expeditionHeader.key).click(expeditionHeader, startLoadNewExpedition);
}

function onUpdateDatabaseExpedition(expeditionHeader){
    $("#editor_expedition_list").find($("#"+expeditionHeader.key)).replaceWith(expeditionListView(expeditionHeader));
    $("#"+expeditionHeader.key).click(expeditionHeader, startLoadNewExpedition);

    if(expeditionHeader.key == getExpeditionEditorContents().key){
        //TODO Modal to ask if you want to load updated expedition or keep editing or save under different name(new key)
        loadExpeditionByKey(expeditionHeader.key);
    }
}

function onRemoveDatabaseExpedition(expeditionHeader){
    $("#editor_expedition_list").find($("#"+expeditionHeader.key)).remove();
}

function startLoadNewExpedition(packedKey){
    var key = null;
    if(packedKey){
        key = packedKey.data.key;
    }
    if(isExpeditionChanged()){
        var onModalComplete = function(){
            loadExpeditionByKey(key);
        };

        $('#modalAskSave').modal("open", {
            dismissible: false,
            complete: onModalComplete
        }).modal("open");
    }
    loadExpeditionByKey(key);
}

//Binds to /expeditions/ , requires larger initial load time and more memory, but loads expeditions quicker
function bindExpeditionListViewPreload(){
    var expeditionsRef = firebase.database().ref('/expeditions/');
    expeditionsRef.off();
    expeditionsRef.on('child_added', function(data) {
        var header = {title:data.val().title, group:data.val().group, email:data.val().email, website:data.val().website, location:data.val().location, departureDate:data.val().departureDate, returnDate:data.val().returnDate, key:data.key};
        onNewDatabaseExpedition(header);
        updateActiveExpeditionView();
        setTimeout(function(){
            subtleFlashElement(data.key, "addedColor", 800, null);
        }, 400);
    });

    expeditionsRef.on('child_changed', function(data) {
        var header = {title:data.val().title, group:data.val().group, email:data.val().email, website:data.val().website, location:data.val().location, departureDate:data.val().departureDate, returnDate:data.val().returnDate, key:data.key};
        onUpdateDatabaseExpedition(header);
        Materialize.toast("<em>" + data.val().title + "</em>&nbsp;has been updated", 4000);
        subtleFlashElement(data.key, "changedColor", 1200, null);
    });

    expeditionsRef.on('child_removed', function(data) {
        var header = {title:data.val().title, group:data.val().group, email:data.val().email, website:data.val().website, location:data.val().location, departureDate:data.val().departureDate, returnDate:data.val().returnDate, key:data.key};
        Materialize.toast("<em>" + data.val().title + "</em>&nbsp;has been removed", 4000);
        subtleFlashElement(data.key, "deletedColor", 1200, function(){
            onRemoveDatabaseExpedition(header);
            updateActiveExpeditionView();
        })
    });
}

function bindAExpeditionLocations(){
    var locationsRef = firebase.database().ref('/location-titles/');
    locationsRef.off();
    locationsRef.on('child_added', function(data) {
        onNewDatabaseExpeditionLocation({title:data.val(), key:data.key});
        // updateActiveArticleView();
        setTimeout(function(){
            subtleFlashElement(data.key, "addedColor", 800, null);
        }, 400);
    });

    locationsRef.on('child_changed', function(data) {
        onUpdateDatabaseExpeditionLocation({title:data.val(), key:data.key});
        Materialize.toast("Location&nbsp;<em>" + data.val() + "</em>&nbsp;has been updated", 4000);
        subtleFlashElement(data.key, "changedColor", 1200, null);
    });

    locationsRef.on('child_removed', function(data) {
        Materialize.toast("Location&nbsp;<em>" + data.val() + "</em>&nbsp;has been removed", 4000);
        subtleFlashElement(data.key, "deletedColor", 1200, function(){
            onRemoveDatabaseExpeditionLocation({title:data.val(), key:data.key});
            // updateActiveArticleView();
        })
    });
}

function expeditionLocationListView(locationHeader){
    return "<option id='" + locationHeader.key + "' value='" + locationHeader.title + "'>" + locationHeader.title + "</option>";
}

function onNewDatabaseExpeditionLocation(locationHeader){
    $("#expedition_location_options").append(expeditionLocationListView(locationHeader));
    sortChildren("#expedition_location_options", "select", function(a,b){
        return $(a).html().localeCompare($(b).html());
    });
    $(document).ready(function() {
        $('select').material_select();
    });
}

function onUpdateDatabaseExpeditionLocation(locationHeader){
    $("#expedition_location_options").find($("#"+locationHeader.key).html(locationHeader.title));
    sortChildren("#expedition_location_options", "select", function(a,b){
        return $(a).html().localeCompare($(b).html());
    });
    $(document).ready(function() {
        $('select').material_select();
    });
}

function onRemoveDatabaseExpeditionLocation(locationHeader){
    $("#expedition_location_options").find($("#"+locationHeader.key)).remove();
    $(document).ready(function() {
        $('select').material_select();
    });
}

function loadExpeditionByKey(key){
    if(null == key){
        newExpedition();
        return;
    }
    setLoading(true);
    firebase.database().ref('/expeditions/' + key).once('value').then(function(snapshot) {
        if(null == snapshot.val()){
            status("error", "failed to retrieve expedition based on key: " +key);
            setLoading(false);
            return;
        }
        var data = {
            title: snapshot.val().title,
            group: snapshot.val().group,
            email: snapshot.val().email,
            website: snapshot.val().website,
            location: snapshot.val().location,
            infotable: snapshot.val().infotable,
            departureDate: snapshot.val().departureDate,
            returnDate: snapshot.val().returnDate,
            key: key
        }
        setExpeditionEditorContents(data);
        setLoading(false);
        status("succes", "loaded expedition: " +snapshot.val().title);

    });
}

function isExpeditionChanged(){
    return !($("#save-expedition-button").hasClass("disabled"));
}

function setExpeditionChanged(val){
    if(!val){
        $("#save-expedition-button").addClass("disabled");
    } else {
        $("#save-expedition-button").removeClass("disabled");
    }
}

function setLoading(isLoading){
    if(isLoading){
        $("#indicator_loading").css('visibility','visible');
    } else {
        $("#indicator_loading").css('visibility','hidden');
    }
}
function deleteDatabaseExpeditionByKey(key){
    backUp('/expeditions/' + key);
    backUp('/expedition-titles/' + key);
    firebase.database().ref('/expeditions/' + key).remove();
    firebase.database().ref('/expedition-titles/' + key).remove();
}

function updateActiveExpeditionView(){
    var key = $("#expedition_key").html();
    $("#editor_expedition_list").find(".active").removeClass("active");
    $("#"+key).addClass("active");
}

function getInfoTableContents(){
    return JSON.stringify({d: expeditionInfoTable.getData()});
}

function setInfoTableContents(data){
    var info = JSON.parse(data);
    expeditionInfoTable.loadData((info.d));
}

function testTable(){
    var data = expeditionInfoTable.getData();
    console.log(data);

    expeditionInfoTable.clear();
    setTimeout(function(){
        expeditionInfoTable.loadData(data);
    }, 700);
}

function getExpeditionEditorContents(){
    return {
        title: $("#expedition_title").val(),
        group: $("#expedition_group").val(),
        email: $("#expedition_email").val(),
        website: $("#expedition_website").val(),
        location: $("#expedition_location_options").val(),
        infotable: getInfoTableContents(),
        departureDate: $("#expedition_departure_date").val(),
        returnDate: $("#expedition_return_date").val(),
        key: $("#expedition_key").html()
    };
}

function setExpeditionEditorContents(expeditionData){
    $("#expedition_key").html(expeditionData.key);
    updateActiveExpeditionView();
    $("#expedition_title").val(expeditionData.title);
    $("#expedition_group").val(expeditionData.group);
    $("#expedition_email").val(expeditionData.email);
    $("#expedition_website").val(expeditionData.website);
    $("#expedition_location_options").val(expeditionData.location).prop('selected', true);
    setInfoTableContents(expeditionData.infotable);
    $("#expedition_departure_date").val(expeditionData.departureDate);
    $("#expedition_return_date").val(expeditionData.returnDate);
    $("#expedition_key").html(expeditionData.key);
    updateActiveExpeditionView();

    //Without setTimeout (delay = 0ms) it is too quick and the contents are loaded after setting it to false, which resets it to true
    //https://stackoverflow.com/questions/779379/why-is-settimeoutfn-0-sometimes-useful
    setTimeout(function(){
        setExpeditionChanged(false);
    }, 0);
}

function saveExpedition(){
    setLoading(true);
    var editorContents = getExpeditionEditorContents();

    var updates = {};
    updates['/expeditions/' + editorContents.key] = editorContents;
    updates['/expedition-titles/' + editorContents.key] = editorContents.title;

    status("succes", "Saved expedition");
    setLoading(false);
    setExpeditionChanged(false);
    return firebase.database().ref().update(updates);
}

function deleteActiveExpedition(){
    deleteDatabaseExpeditionByKey(getExpeditionEditorContents().key);
    newExpedition();
}

function initExpeditionSafeguards(){
    $("#expedition_title").on("input", function(){
        setExpeditionChanged(true);
    });

    $("#expedition_group").on("input", function(){
        setExpeditionChanged(true);
    });

    $("#expedition_website").on("input", function(){
        setExpeditionChanged(true);
    });

    $("#expedition_email").on("input", function(){
        setExpeditionChanged(true);
    });

    $("#expedition_location").on("input", function(){
        setExpeditionChanged(true);
    });

    $("#expedition_departure_date").on("input change", function(){
        setExpeditionChanged(true);
    });

    $("#expedition_return_date").on("input change", function(){
        setExpeditionChanged(true);
    });

    $(window).bind('beforeunload', function (e) {
        if(isExpeditionChanged()){
            //This results in a box asking to stay/leave with the text you have unsaved changes
            return "You have unsaved changes";
        }
        return;
    });
}

function emptyTableData(){
    return [[]];
}

function initUiComponents(){
    // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();

    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15 // Creates a dropdown of 15 years to control year
    });

    $(document).ready(function() {
        $('select').material_select();
    });

    var tableContainer = document.getElementById("#expedtion_info_editor");
    expeditionInfoTable = new Handsontable(tableContainer, {
        data: emptyTableData(),
        columns: [
            {type : 'text'},
            {type : 'text'},
            {type : 'text'}
        ],
        minSpareCols: 0,
        minSpareRows: 1,
        autoWrapRow: true,
        stretchH: 'all',
        contextMenu: true,
        rowHeaders: true,
        colHeaders: [
            'Name',
            'Number',
            'Email'
        ],
        beforeChange:function (change, source) {
            setExpeditionChanged(true);
        }
    });

}

function initExpeditionEditor(){
    bindExpeditionListViewPreload();
    bindAExpeditionLocations();
    initUiComponents();
    newExpedition();
    initExpeditionSafeguards();
    setLoading(false);
}
