function status(status, text){
  console.log(status + ": " + text);
  document.getElementById("status").className = status;
  document.getElementById("status").innerHTML = text + "<span style='float:right; color:#888;'>" + new Date() + "</span>";
}

function logOutput(){
  var title = document.getElementById("article_title").value;
  var articleHtml = quill.root.innerHTML.trim();
  console.log("Title: " + title + "\n Body: " + articleHtml);
  status("succes", "logged html to console");
}
function testFlash(){
    subtleFlashElement("test-flash-button", "deletedColor", 1200, function(){
        console.log("testFlash callback called");
    });
}

function subtleFlashElement(id, className, duration, callback){
    $("#"+id).addClass(className);
    setTimeout(function(){
        $("#"+id).removeClass(className);
        if(null != callback){
            callback();
        }
    }, duration);
}
function setLoading(isLoading){
    if(isLoading){
        $("#indicator_loading").css('visibility','visible');
    } else {
        $("#indicator_loading").css('visibility','hidden');
    }
}

function copyKey(from, to){
    firebase.database().ref(from).once('value').then(function(snapshot) {
        if(null == snapshot.val()){
            status("error", "failed to retrieve " + from);
            setLoading(false);
            return;
        }
        var updates = {};
        updates[to] = snapshot.val();
        status("succes", "moved " + from + " to " + to);
        return firebase.database().ref().update(updates);
    });
}

function backUp(key){
    copyKey(key, "/backup/" +key);
}

function getBase64ImageDimensions(base64){
    var i = new Image();
    i.src=base64;
    return ({width: i.width, height: i.height});
}

function copyKeyFromTextfields(){
    var from = $("#copy-key-from").val();
    var to = $("#copy-key-to").val();
    if("" == from){
        subtleFlashElement("copy-key-from", "error", 1200, null);
    }
    if("" == to){
        subtleFlashElement("copy-key-to", "error", 1200, null);
    }
    if("" != from && "" != to){
        copyKey(from, to);
    }
}

function init(editor){
    $("#nav-container").children().removeClass("active");
    $("a[data-editor-ref=editor]").parent().addClass("active");
    switch(editor){
        case "articleEditor":
            initArticleEditor();
            break;
        case "expeditionEditor":
            initExpeditionEditor();
            break;
        case "locationEditor":
            initLocationEditor();
            break;
        default:
            console.log("No matches for init:" + editor);
    }
    setLoading(false);
}

function loadEditor(htmlFileName){
    $( "#editor_wrapper" ).load( htmlFileName + ".html", function() {
        console.log("loaded " + htmlFileName);
        init(htmlFileName);
    });
}

function sortChildrenTest(){
    sortChildren("#editor_article_list", "a", function(a, b){
        return a.innerHTML.localeCompare(b.innerHTML);
    });
}

function sortChildren(parentSelector, childSelector, compare){
    $(parentSelector + " " + childSelector).sort(compare).appendTo(parentSelector);
}

document.addEventListener('DOMContentLoaded', function() {
  status("default", "started");
  // // 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥
  // // The Firebase SDK is initialized and available here!
  //
  // firebase.auth().onAuthStateChanged(user => { });
  // firebase.database().ref('/path/to/ref').on('value', snapshot => { });
  // firebase.messaging().requestPermission().then(() => { });
  // firebase.storage().ref('/path/to/ref').getDownloadURL().then(() => { });
  //
  // // 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥


  try {
    setLoading(true);
    let app = firebase.app();
    let features = ['auth', 'database', 'messaging', 'storage'].filter(feature => typeof app[feature] === 'function');
    
    //Firebase sdk loaded
    status("succes", "Firebase sdk loaded with " + features.join(', '));

    //Active editor onload
    loadEditor("articleEditor");

  } catch (e) {
    console.error(e);
    status("error", "Firebase sdk failed to load");
  }
});


// var articleJson = html2json(articleHtml);
// console.log(articleJson);
// console.log(JSON.stringify(articleJson, ' ', ' '));