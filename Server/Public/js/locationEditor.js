function getNewLocationKey(){
    return firebase.database().ref().child('locations').push().key;
}

function newLocation(){
    setLocationEditorContents({title:"", body:"<p><br></p>", key:getNewLocationKey()});
    $("#location_title").focus();
    status("succes", "New location");
}

function locationListView(locationHeader){
    return "<a id='" + locationHeader.key + "' href='#' class='collection-item'>"+ locationHeader.title +"</a>";
}

function onNewDatabaseLocation(locationHeader){
    $("#editor_location_list").append(locationListView(locationHeader));
    // $("#"+locationHeader.key).click(locationHeader, loadArticleByPackedKey);
    $("#"+locationHeader.key).click(locationHeader, startLoadNewLocation);
}

function onUpdateDatabaseLocation(locationHeader){
    $("#editor_location_list").find($("#"+locationHeader.key)).html(locationHeader.title);
    if(locationHeader.key == getLocationEditorContents().key){
        //TODO Modal to ask if you want to load update location or keep editing or save under different name(new key)
        loadLocationByKey(locationHeader.key);
    }
}

function onRemoveDatabaseLocation(locationHeader){
    $("#editor_location_list").find($("#"+locationHeader.key)).remove();
}

function loadLocationByPackedKey(packedKey){
    loadLocationByKey(packedKey.data.key);
}

function startLoadNewLocation(packedKey){
    var key = null;
    if(packedKey){
        key = packedKey.data.key;
    }
    if(isLocationChanged()){
        var onModalComplete = function(){
            loadLocationByKey(key);
        };

        $('#modalAskSave').modal("open", {
            dismissible: false,
            complete: onModalComplete
        }).modal("open");
    }
    loadLocationByKey(key);
}


//Binds to /titles/, quicker initial load and less memory, but individual location load time increased
function bindLocationListView(){
    var titlesRef = firebase.database().ref('/location-titles/');
    titlesRef.off();
    titlesRef.on('child_added', function (data) {
        onNewDatabaseLocation({title: data.val(), key: data.key});
        titlesRef.on('child_added', function(data) {
            onNewDatabaseLocation({title:data.val().title, key:data.key});
            updateActiveLocationView();
            setTimeout(function(){
                subtleFlashElement(data.key, "addedColor", 800, null);
            }, 400);
        });
    });

    titlesRef.on('child_changed', function (data) {
        onUpdateDatabaseLocation({title: data.val(), key: data.key});
        Materialize.toast("<em>" + data.val() + "</em>&nbsp;has been updated", 4000);
        subtleFlashElement(data.key, "changedColor", 1200, null);
    });

    titlesRef.on('child_removed', function(data) {
        Materialize.toast("<em>" + data.val().title + "</em>&nbsp;has been removed", 4000);
        subtleFlashElement(data.key, "deletedColor", 1200, function () {
            onRemoveDatabaseLocation({title: data.val().title, key: data.key});
            updateActiveLocationView();
        });
    });
}

//Binds to /locations/ , requires larger initial load time and more memory, but loads locations quicker
function bindLocationListViewPreload(){
    var locationsRef = firebase.database().ref('/locations/');
    locationsRef.off();
    locationsRef.on('child_added', function(data) {
        onNewDatabaseLocation({title:data.val().title, key:data.key});
        updateActiveLocationView();
        setTimeout(function(){
            subtleFlashElement(data.key, "addedColor", 800, null);
        }, 400);
    });

    locationsRef.on('child_changed', function(data) {
        onUpdateDatabaseLocation({title:data.val().title, key:data.key});
        Materialize.toast("<em>" + data.val().title + "</em>&nbsp;has been updated", 4000);
        subtleFlashElement(data.key, "changedColor", 1200, null);
    });

    locationsRef.on('child_removed', function(data) {
        Materialize.toast("<em>" + data.val().title + "</em>&nbsp;has been removed", 4000);
        subtleFlashElement(data.key, "deletedColor", 1200, function(){
            onRemoveDatabaseLocation({title:data.val().title, key:data.key});
            updateActiveLocationView();
        })
    });
}

function bindLocations(){
    if(config.locationLoadMode == "preload"){
        bindLocationListViewPreload();
    } else {
        bindLocationListView();
    }
}

function loadLocationByKey(key){
    if(null == key){
        newLocation();
        return;
    }
    setLoading(true);
    firebase.database().ref('/locations/' + key).once('value').then(function(snapshot) {
        if(null == snapshot.val()){
            status("error", "failed to retrieve location based on key: " +key);
            setLoading(false);
            return;
        }
        var data = {
            title: snapshot.val().title,
            // body: snapshot.val().body,
            key: key
        }
        setLocationEditorContents(data);
        setLoading(false);
        status("succes", "loaded location: " +snapshot.val().title);

    });
}

function isLocationChanged(){
    return !($("#save-location-button").hasClass("disabled"));
}

function setLocationChanged(val){
    if(!val){
        $("#save-location-button").addClass("disabled");
    } else {
        $("#save-location-button").removeClass("disabled");
    }
}

function setLoading(isLoading){
    if(isLoading){
        $("#indicator_loading").css('visibility','visible');
    } else {
        $("#indicator_loading").css('visibility','hidden');
    }
}
function deleteDatabaseLocationByKey(key){
    backUp('/locations/' + key);
    backUp('/location-titles/' + key);
    firebase.database().ref('/locations/' + key).remove();
    firebase.database().ref('/location-titles/' + key).remove();
}

function updateActiveLocationView(){
    var key = $("#location_key").html();
    $("#editor_location_list").find(".active").removeClass("active");
    $("#"+key).addClass("active");
}

function getLocationEditorContents(){
    return {
        title: $("#location_title").val(),
        // body: quill.root.innerHTML,
        key: $("#location_key").html()
    };
}

function setLocationEditorContents(locationData){
    $("#location_title").val(locationData.title);
    // quill.root.innerHTML = locationData.body;
    $("#location_key").html(locationData.key);
    updateActiveLocationView();

    //Without setTimeout (delay = 0ms) it is too quick and the contents are loaded after setting it to false, which resets it to true
    setTimeout(function(){
        // console.log("timeoutboy");
        setLocationChanged(false);
    }, 0);
}

function saveLocation(){
    setLoading(true);
    var editorContents = getLocationEditorContents();

    var updates = {};
    updates['/locations/' + editorContents.key] = editorContents;
    updates['/location-titles/' + editorContents.key] = editorContents.title;

    status("succes", "Saved location");
    setLoading(false);
    setLocationChanged(false);
    return firebase.database().ref().update(updates);
}

function deleteActiveLocation(){
    deleteDatabaseLocationByKey(getLocationEditorContents().key);
    newLocation();
}

function initLocationSaveGuards(){
    // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();

    $("#location_title").on("input", function(){
        setLocationChanged(true);
    });

    $(window).bind('beforeunload', function (e) {
        if(isLocationChanged()){
            //This results in a box asking to stay/leave with the text you have unsaved changes
            return "aaa";
        }
        return;
    });
}

function initLocationEditor(){
    bindLocations();
    newLocation();
    initLocationSaveGuards();
    setLoading(false);
}
