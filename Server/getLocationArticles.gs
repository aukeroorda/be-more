function c(){
  return {
  LOCATION_SHEET_NAME: 'locations',
  CONTINENT_SHEET_NAME: 'continents',
  ARTICLE_SHEET_NAME: 'articles',
  varibale2: 2
  }
}

function testDoGet(){
  e = {parameter: {location: 'Malawi'}};
  doGet(e);
}

function doGet(e) {
  
  if(typeof e == 'undefined'){
    Logger.log('e == undefined');
  }
  
  const location = e.parameter['location'];
  if(location == ''){
    Logger.log('location == %d', e.parameter['location']);
    return;
  }
  
  var articles = getArticles(getArticleIds(location));
  
  return ContentService.createTextOutput(JSON.stringify(articles))
  .setMimeType(ContentService.MimeType.JSON);
}

function getArticles(articleIds){
  if(!articleIds){
    return;
  }
  //Sheet
  const spreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  const articleSheet = spreadSheet.getSheetByName(c().ARTICLE_SHEET_NAME);
  const articleData = articleSheet.getDataRange().getValues();
  
  var articles = [];
  
  articleIds.forEach(function(id){
    if(articleData[id][0] || articleData[id][0] == 0){
      var article = {
        "title" : articleData[id][1], 
        "images" : articleData[id][2],
        "text" : articleData[id][3]
      };
      articles.push(article);
    }
  });
  return articles;
}

function getArticleIds(location){
  //Sheet
  const spreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  const locationSheet = spreadSheet.getSheetByName(c().LOCATION_SHEET_NAME);
  const locationData = locationSheet.getDataRange().getValues();
  
  var articleIds = [];
  
  for(var i=1; i<locationData.length; i++){
    if(locationData[i][1] == location){
      var directArticles = parseArticleString(locationData[i][2]);
      var continentArticles = getContinentArticles(locationData[i][3]);
      
      articleIds = articleIds.concat(directArticles);
      articleIds = articleIds.concat(continentArticles);
      articleIds = uniqueArray(articleIds);
      Logger.log(articleIds);
      return articleIds;
    }
  }
}

function parseArticleString(dataArray){
  if(isString(dataArray)){
    var data = removeWhitespace(dataArray);
    return data.split(',');
  } else {
    return data.toString().split(',');
  }
}


function getContinentArticles(continent){
  //Sheet
  const spreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  const continentSheet = spreadSheet.getSheetByName(c().CONTINENT_SHEET_NAME);
  const continentData = continentSheet.getDataRange().getValues();
  for(var i=1; i<continentData.length; i++){
    if(continentData[i][1] == continent){
      return parseArticleString(continentData[i][2]);
    }
  }
}

//src: https://stackoverflow.com/a/43046408/3684659
function uniqueArray( ar ) {
  var j = {};

  ar.forEach( function(v) {
    j[v+ '::' + typeof v] = v;
  });

  return Object.keys(j).map(function(v){
    return j[v];
  });
} 

function isString(val){
  return typeof val === 'string';
}

function removeWhitespace(val){
  return val.replace(/\s+/g, '');
}
