function myFunction() {
  
  var AUTOINC_COLUMN = 0;
  var INDEX_OFFSET = 0;
  
  var SHEETS = ["continents", "locations", "articles", "images"];
                
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
                
  for(var i = 0; i < SHEETS.length; i++){
                
    var worksheet   = spreadsheet.getSheetByName(SHEETS[i]);
    var rows        = worksheet.getDataRange().getNumRows();
    var vals        = worksheet.getSheetValues(1, 1, rows, 2);
  
              
    for (var row = INDEX_OFFSET; row < vals.length; row++) {
      try {
        var id = vals[row][AUTOINC_COLUMN];
        Logger.log(id);Logger.log((""+id).length ===0);
        if ((""+id).length === 0) {
          // Here the columns & rows are 1-indexed
          worksheet.getRange(row+1, AUTOINC_COLUMN+1).setValue(row-1);
        }
      } catch(ex) {
        // Keep calm and carry on
      }
    }           
  }
}