package nl.aukeroorda.bemore.db;

import android.content.Context;

/**
 * Created by Auke on 29-6-2017.
 */

public class DownloadArguments {

    private Context context;
    private String location;

    public DownloadArguments(Context c) {
        this.context = c;
    }

    public DownloadArguments(Context c, String location) {
        this.context = c;
        this.location = location;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}