package nl.aukeroorda.bemore;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;

import nl.aukeroorda.bemore.ArticleFragment.OnListFragmentInteractionListener;
import nl.aukeroorda.bemore.db.Article;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Article} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyArticleRecyclerViewAdapter extends RecyclerView.Adapter<MyArticleRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private final String LOG_TAG = "MyArticleRVA";
    private final List<Article> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyArticleRecyclerViewAdapter(List<Article> items, OnListFragmentInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_article, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Article article = mValues.get(position);
        holder.mItem = mValues.get(position);
        holder.mTitleView.setText(article.getTitle());
        holder.mContentView.setText(article.getText());
        if(article.getImageRefs().size() > 0){
            Log.d(LOG_TAG, article.getImageRefs().get(0).toString());
            Glide.with(context)
                    .using(new FirebaseImageLoader())
                    .load(article.getImageRefs().get(0))
                    .into(holder.mImageView);
        }

        holder.mImageTitleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
        holder.mLearnMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    public void updateArticles(ArrayList<Article> articles){
        mValues.clear();
        mValues.addAll(articles);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mTitleView;
        public final RelativeLayout mImageTitleView;
        public final TextView mContentView;
        public final Button mLearnMoreButton;
        public Article mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.image_view);
            mTitleView = (TextView) view.findViewById(R.id.title_view);
            mImageTitleView = (RelativeLayout) view.findViewById(R.id.image_title_view);
            mContentView = (TextView) view.findViewById(R.id.description);
            mLearnMoreButton = (Button) view.findViewById(R.id.learn_more_button);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
