package nl.aukeroorda.bemore;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;

import java.util.ArrayList;

import nl.aukeroorda.bemore.db.Article;

public class ArticleActivity extends AppCompatActivity {

    private final String LOG_TAG = "ArticleActivity";
    private Article article;
    private ArrayList<String> textSegments;

    private Toolbar toolbar;
    private ImageView toolbarImageView;

    private NestedScrollView nestedScrollView;
    private LinearLayout linearLayout;
    private TextView contentTextView;
    private TextView htmlTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarImageView = (ImageView) findViewById(R.id.toolbar_image_view);
        nestedScrollView = (NestedScrollView) findViewById(R.id.content_article);
        linearLayout = (LinearLayout) nestedScrollView.findViewById(R.id.linear_layout);
        contentTextView = (TextView) linearLayout.findViewById(R.id.text_view);
        htmlTextView = (TextView) linearLayout.findViewById(R.id.htmltext);

        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        article = Article.retrieveArticleData(intent);
        textSegments = article.getTextSegments();

        Glide.with(this)
                .using(new FirebaseImageLoader())
                .load(article.getImageRefs().get(0))
                .into(toolbarImageView);
        toolbar.setTitle(article.getTitle());

        htmlTextView.setText(fromHtml("<h1>h1 Header</h1><p>Hello <em>World!</em></p><h2>h2 Header</h2><p>paragraph p</p>"));

        //Create views for paragraphs and images
        contentTextView.setText(textSegments.get(0));
        for(int i=1; i<article.getTextSegments().size(); i++){
            LayoutInflater li=(LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            TextView textView = (TextView) li.inflate(R.layout.margin_text_view, linearLayout, false);
            textView.setText(textSegments.get(i));

            ImageView imageView = new ImageView(this);
            Glide.with(this)
                .using(new FirebaseImageLoader())
                .load(article.getImageRefs().get(i%(article.getImageRefs().size())))
                .into(imageView);

            linearLayout.addView(imageView);
            linearLayout.addView(textView);
        }
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
