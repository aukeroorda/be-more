package nl.aukeroorda.bemore.db;

import android.content.Intent;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Auke on 29-6-2017.
 */

public class Article{
    private int id;
    private String title;
    private ArrayList<Integer> images;
    private ArrayList<StorageReference> imageRefs = new ArrayList<>();
    private String text;

    public Article(int id, String title, ArrayList<Integer> images, String text) {
        this.id = id;
        this.title = title;
        this.images = images;
        this.imageRefs = generateImageRefs(images);
        this.text = text;
    }

    public static Intent storeArticleData(Intent i, Article a){
        i.putExtra("id", a.getId());
        i.putExtra("title", a.getTitle());
        i.putExtra("images", a.getImages());
        i.putExtra("text", a.getText());
        return i;
    }

    public static Article retrieveArticleData(Intent i){
        return new Article(i.getIntExtra("id", 0),
                        i.getStringExtra("title"),
                        i.getIntegerArrayListExtra("images"),
                        i.getStringExtra("text"));
    }

    public ArrayList<StorageReference> generateImageRefs(ArrayList<Integer> images){
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();

        ArrayList<StorageReference> refs = new ArrayList<>();
        for(Integer i : images){
            if(null != i) {
                StorageReference imageReference = storageReference.child("images/" + Integer.toString(i) + ".jpg");
                refs.add(imageReference);
            }
        }

        if(0 == refs.size()){
            StorageReference imageReference = storageReference.child("images/" + Integer.toString(0) + ".jpg");
            refs.add(imageReference);
        }

        return refs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        if (getId() != article.getId()) return false;
        if (getTitle() != null ? !getTitle().equals(article.getTitle()) : article.getTitle() != null)
            return false;
        if (getImages() != null ? !getImages().equals(article.getImages()) : article.getImages() != null)
            return false;
        return getText() != null ? getText().equals(article.getText()) : article.getText() == null;

    }

    @Override
    public int hashCode() {
        return getId();
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<Integer> getImages() {
        return images;
    }

    public ArrayList<StorageReference> getImageRefs() {
        return imageRefs;
    }

    public StorageReference getRandomImageRef(){
        //TODO Make random
        return getImageRefs().get(0);
    }

    public String getText() {
        return text;
    }

    public ArrayList<String> getTextSegments(){
        ArrayList<String> arr = new ArrayList<>();
        String[] strings = getText().split("\\r?\\n\\r?\\n");
        arr.addAll(Arrays.asList(strings));
        return arr;
    }
}
