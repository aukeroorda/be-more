package nl.aukeroorda.bemore.db;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import nl.aukeroorda.bemore.BuildConfig;
import nl.aukeroorda.bemore.MainActivity;
import nl.aukeroorda.bemore.R;

/**
 * Created by Auke on 29-6-2017.
 */

public class DatabaseDownloader extends AsyncTask<DownloadArguments, Void, String> {

    private final String LOG_TAG = "DatabaseDownloader";

    private BufferedReader in = null;

    private Context context;
    private ArrayList<Article> articles = new ArrayList<>();
    private String API_PARAM = "location";
    private String location = "Oeganda";

    protected void onPreExecute(){}

    @Override
    protected String doInBackground(DownloadArguments... params) {

        this.context = params[0].getContext();
        if(null != params[0].getLocation()){
            location = params[0].getLocation();
        }
        HttpClient client = new DefaultHttpClient();


        List<BasicNameValuePair> nvp = new ArrayList<>(1);
        nvp.add(new BasicNameValuePair(API_PARAM,location));


        HttpGet get = new HttpGet(context.getResources().getString(R.string.API_URL) +
                URLEncodedUtils.format(nvp, HTTP.UTF_8));

        try {
            HttpResponse response = client.execute(get);
            Log.d(LOG_TAG,response.getStatusLine().toString());
            in = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()
            ));
        } catch (IOException e) {
            e.printStackTrace();
        }

        StringBuilder builder = new StringBuilder();
        String temp;
        try {
            while((temp = in.readLine())!= null){
                builder.append(temp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    protected void onPostExecute(String result) {

        DatabaseConnection dbc= new DatabaseConnection(this.context);
        Gson gson = new GsonBuilder().serializeNulls().create();

//        if(BuildConfig.DEBUG){
            Log.i(LOG_TAG, "DBDownload onPostExecute()");
            Log.i(LOG_TAG, result);
//        }

        Type listType = new TypeToken<ArrayList<Article>>(){}.getType();
        ArrayList<Article> articles = gson.fromJson(result, listType);
        dbc.overwriteArticles(articles);
        this.articles = new ArrayList<>();
        this.articles.addAll(articles);

        if(BuildConfig.DEBUG){
        ArrayList<Article> q2=dbc.getArticles(new ArrayList<Integer>());
            if(!(articles.equals(q2))){
                Log.e(LOG_TAG, "Retrieved articles are different from just inserted articles. Throwing AssertionError.");
                throw new AssertionError();
            }
        }
    }
}
