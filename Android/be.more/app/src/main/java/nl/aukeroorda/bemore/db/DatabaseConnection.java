package nl.aukeroorda.bemore.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Auke on 28-6-2017.
 */

public class DatabaseConnection extends SQLiteOpenHelper {

    private final Context context;
    private static final String DATABASE_NAME = "be.more.db";


//    private static final String TELEPHONE_TABLE_NAME = "TelephoneData";
    private static final String ARTICLE_TABLE_NAME = "ArticleData";

//    private static final String TELEPHONE_COLUMN_ID = "TelephoneID";
//    private static final String TELEPHONE_COLUMN_NAME = "TelephoneName";
//    private static final String TELEPHONE_COLUMN_NUMBER = "TelephoneNumber";
    private static final String ARTICLE_COLUMN_ID = "ArticleID";
    private static final String ARTICLE_COLUMN_TITLE = "ArticleTitle";
    private static final String ARTICLE_COLUMN_IMAGES = "ArticleImages";
    private static final String ARTICLE_COLUMN_TEXT = "ArticleText";
//
//    private static final String CREATE_TABLE_TELEPHONE = "CREATE TABLE " + TELEPHONE_TABLE_NAME +
//            TELEPHONE_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
//            TELEPHONE_COLUMN_NAME + " TEXT," +
//            TELEPHONE_COLUMN_NUMBER + " TEXT" + ")";
    private static final String CREATE_TABLE_ARTICLE = "CREATE TABLE " + ARTICLE_TABLE_NAME + "(" +
        ARTICLE_COLUMN_ID + " INTEGER PRIMARY KEY NOT NULL," +
        ARTICLE_COLUMN_TITLE + " TEXT," +
        ARTICLE_COLUMN_IMAGES + " TEXT," +
        ARTICLE_COLUMN_TEXT + " TEXT" + ")";


    public DatabaseConnection(Context context) {
        super(context, DATABASE_NAME, null, 2);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//        db.execSQL(CREATE_TABLE_TELEPHONE);
        db.execSQL(CREATE_TABLE_ARTICLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
//        db.execSQL("DROP TABLE IF EXISTS " + TELEPHONE_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ARTICLE_TABLE_NAME);
        // create new tables
        onCreate(db);
    }

    public void overwriteArticles(ArrayList<Article> articles){
        SQLiteDatabase db = this.getWritableDatabase();
        //Drop and recreate to reset possible auto-increments
        db.execSQL("DROP TABLE IF EXISTS " + ARTICLE_TABLE_NAME);
        db.execSQL(CREATE_TABLE_ARTICLE);
        db.close();
        insertArticles(articles);
    }

    public void insertArticles(ArrayList<Article> articles){
        SQLiteDatabase db = this.getWritableDatabase();
        Gson gson = new Gson();

        for(Article a : articles){
            ContentValues cv = new ContentValues();
            cv.put(ARTICLE_COLUMN_ID, a.getId());
            cv.put(ARTICLE_COLUMN_TITLE, a.getTitle());
            cv.put(ARTICLE_COLUMN_IMAGES, gson.toJson(a.getImages()));
            cv.put(ARTICLE_COLUMN_TEXT, a.getText());

            db.insert(ARTICLE_TABLE_NAME, null, cv);
        }

        db.close();
    }

    public ArrayList<Article> getArticles(ArrayList<Integer> articleIds){
        SQLiteDatabase db = this.getWritableDatabase();
        Gson gson = new Gson();
        Type intListType = new TypeToken<ArrayList<Integer>>(){}.getType();

        ArrayList<Article> articles = new ArrayList<>();


        String articleQuery;
        if(0 == articleIds.size()){
            articleQuery = "select * from " + ARTICLE_TABLE_NAME;
        } else {
            articleQuery = "select * from " + ARTICLE_TABLE_NAME + " where " + ARTICLE_COLUMN_ID + " in (" + queryFormatIntegers(articleIds) + ")";
        }
        Cursor res = db.rawQuery(articleQuery, null);

        if(res.moveToFirst()) {
            do {
                //Decode the images array
                ArrayList<Integer> images = gson.fromJson(res.getString(res.getColumnIndex(ARTICLE_COLUMN_IMAGES)), intListType);

                Article a = new Article(res.getInt(res.getColumnIndex(ARTICLE_COLUMN_ID)),
                            res.getString(res.getColumnIndex(ARTICLE_COLUMN_TITLE)),
                            images,
                            res.getString(res.getColumnIndex(ARTICLE_COLUMN_TEXT))
                        );
                articles.add(a);
            } while (res.moveToNext());
        }

        res.close();
        db.close();
        return articles;
    }

    private String queryFormatIntegers(ArrayList<Integer> articleIds){
        StringBuilder sb = new StringBuilder();
        for(Integer i : articleIds){
            sb.append("\'" + i + "\'");
            sb.append(",");
        }
        return sb.length() > 0 ? sb.substring(0, sb.length()-1): "";
    }
}
