package nl.aukeroorda.bemore;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import nl.aukeroorda.bemore.db.Article;
import nl.aukeroorda.bemore.db.DatabaseConnection;
import nl.aukeroorda.bemore.db.DatabaseDownloader;
import nl.aukeroorda.bemore.db.DownloadArguments;

public class MainActivity extends AppCompatActivity implements ArticleFragment.OnListFragmentInteractionListener{

    private final String LOG_TAG = "MainActivity LOG TAG";
    private TextView mTextMessage;
    private FrameLayout frameLayout;

    private FirebaseAuth mAuth;

    private StorageReference mStorageRef;
    private StorageReference safariRef;

    private DatabaseDownloader dbd = new DatabaseDownloader();
    private DatabaseConnection dbc = new DatabaseConnection(this);

    private RecyclerView articleRecyclerView;
    private MyArticleRecyclerViewAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Article> articles = new ArrayList<>();


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
//                    mTextMessage.setText(R.string.title_home);
                    getSupportActionBar().setTitle(R.string.title_home);
                    adapter.notifyDataSetChanged();
                    setFrameLayoutView(frameLayout, articleRecyclerView);
                    return true;
                case R.id.navigation_dashboard:
                    new DatabaseDownloader().execute(new DownloadArguments(getApplicationContext()));
                    setFrameLayoutView(frameLayout, mTextMessage);
                    mTextMessage.setText(R.string.title_dashboard);
                    getSupportActionBar().setTitle(R.string.title_dashboard);
                    return true;
                case R.id.navigation_notifications:
                    setFrameLayoutView(frameLayout, mTextMessage);
                    mTextMessage.setText(R.string.title_notifications);
                    getSupportActionBar().setTitle(R.string.title_notifications);
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        dbd.execute(new DownloadArguments(this));

        mTextMessage = (TextView) findViewById(R.id.message);
        frameLayout = (FrameLayout) findViewById(R.id.content);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        articleRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        layoutManager = new LinearLayoutManager(this);
        articleRecyclerView.setLayoutManager(layoutManager);

        adapter = new MyArticleRecyclerViewAdapter(articles, this, this);
        articleRecyclerView.setAdapter(adapter);
        adapter.updateArticles(dbc.getArticles(new ArrayList<Integer>()));


        getSupportActionBar().setTitle(R.string.title_home);

    }

    @Override
    public void onStart(){
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            //Firebase authorised
        } else {
            signInAnonymously();
        }
    }

    private void signInAnonymously() {
        mAuth.signInAnonymously().addOnSuccessListener(this, new  OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                Log.i("AnonymousSignIn", "Anonymous sign in succesful");
            }
        })
            .addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                Log.e("AnonymousSignIn", "signInAnonymously:FAILURE", exception);
                }
            });
    }

    public void loadImage(){
//        Glide.with(this)
//                .using(new FirebaseImageLoader())
//                .load(safariRef)
//                .into(testImage);
////        testImage.setVisibility(View.VISIBLE);
//        testImage.setAdjustViewBounds(true);
//        testImage.setScaleType(ImageView.ScaleType.FIT_XY);
//        testImage.invalidate();
    }

    void setFrameLayoutView(FrameLayout fl, View v){
        fl.removeAllViewsInLayout();
        if(null != v) {
            fl.addView(v);
        }
    }



    public void onListFragmentInteraction(Article article){
        Toast.makeText(this, article.getTitle(), Toast.LENGTH_SHORT).show();
        Intent intent=new Intent(this.getApplicationContext(),ArticleActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent = Article.storeArticleData(intent, article);
        startActivity(intent);
    }
}
